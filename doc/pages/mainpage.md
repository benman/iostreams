@mainpage

@tableofcontents

Here is a very basic example reading line by line a text file

@code{.cpp}
#include <iostream/iostream.hpp>
#include <iostream/file_stream.hpp>
#include <iostream/buffer_stream.hpp>
#include <iostream>

int main(int argc, char **argv) {
    std::string testFile = TEST_DIR;
    testFile += "/testFile.txt";
    std::string fileName = argc == 2? argv[1] : testFile;
    std::cout << "openning " << fileName << "\n";


    // Most streams are not buffered by default. You must wrap a buffer yourself.
    ios::buffer_istream<ios::file_istream> stream(std::move(ios::file_istream(fileName)));

    // loop through each line
    for(std::string line : ios::line_itext<decltype(stream)>(std::move(stream))) {
        // do something with line. "\n" is not appended to line variable.
        std::cout << line << "\n";
    }

}

@endcode
