#[[
Requirements
    BINARY_DIR to point to the binary directory of where output should go
    PROJECT_VERSION project version
    PROJECT_SOURCE_DIR
    BASE_NAME
#]]

string(REPLACE "." "-" suffix ${PROJECT_VERSION})

set(baseDir ${BINARY_DIR}/${BASE_NAME})
execute_process(COMMAND ${CMAKE_COMMAND} -E remove_directory ${srcFile} ${baseDir}/src/${fileName})
file(MAKE_DIRECTORY ${baseDir})

configure_file(${PROJECT_SOURCE_DIR}/CMakeLists.txt ${baseDir}/CMakeLists.txt COPYONLY)
configure_file(${PROJECT_SOURCE_DIR}/License.txt ${baseDir}/License.txt COPYONLY)
configure_file(${PROJECT_SOURCE_DIR}/README.md ${baseDir}/README.md COPYONLY)

file(MAKE_DIRECTORY ${baseDir}/src)

file(GLOB srcFiles "${PROJECT_SOURCE_DIR}/src/**")

foreach(srcFile ${srcFiles})
    get_filename_component(fileName ${srcFile} NAME)
    execute_process(COMMAND ${CMAKE_COMMAND} -E copy_directory ${srcFile} ${baseDir}/src/${fileName})
endforeach()

file(REMOVE ${BINARY_DIR}/${BASE_NAME}.tgz)
execute_process(COMMAND
    ${CMAKE_COMMAND} -E tar cfz ${BASE_NAME}.tgz ${baseDir}
    WORKING_DIRECTORY ${BINARY_DIR}
)
