#[[
Requirements
    BINARY_DIR to point to the binary directory of where output should go
    DOC_DIR dir of documentation configuration files
    PROJECT_VERSION project version
#]]
set(doxyFile ${BINARY_DIR}/DoxyFile)
configure_file("${DOC_DIR}/Doxyfile" "${doxyFile}" COPYONLY)

file(APPEND ${doxyFile} "OUTPUT_DIRECTORY=${BINARY_DIR}/doc\n")
file(APPEND ${doxyFile} "PROJECT_NUMBER=${PROJECT_VERSION}\n")

execute_process(COMMAND doxygen ${doxyFile}
    WORKING_DIRECTORY ${DOC_DIR}
)
