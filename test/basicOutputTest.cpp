#include <iostream>
#include <type_traits>
#include <sys/types.h>
#include <unistd.h>
#include <cxxtest/TestSuite.h>

#include <iostream/async_stream.hpp>
#include <iostream/file_stream.hpp>
#include <iostream/block_stream.hpp>
#include <iostream/buffer_stream.hpp>
#include <iostream/virtual_stream.hpp>

#include <iostream/iostream.hpp>
#include <iostream/sub_stream.hpp>
#include <iostream/stream_utility.hpp>


#include "common.hpp"

using namespace ios;

class Fixture {
public:
    Fixture() {
        stringTestFile = testDir("testFile.txt");
        tmpOutputFile = tmpDir("tmpOutput");
    }
    std::string stringTestFile;
    std::string tmpOutputFile;
};
class MyTestSuite : public CxxTest::TestSuite
{
public:

    // CXXTest doesn't allow constructors & destructors for a suite
    static MyTestSuite *createSuite() {
        MyTestSuite *suite = new MyTestSuite();
        suite->init();
        return suite;
    }

    static void destroySuite(MyTestSuite *suite) {
        suite->release();
        delete suite;
    }

    void init() {
        mGlobal = new Fixture();
    }

    void release() {
        delete mGlobal;
    }

    void test_file_ostream() {
        std::vector<uint8_t> testFileData = read_file(mGlobal->stringTestFile);
        {
            file_ostream output(mGlobal->tmpOutputFile);
            TS_ASSERT_EQUALS(output.is_open(), true);
            std::streamsize written = output.write(testFileData.data(), testFileData.size());
            TS_ASSERT_EQUALS(written, testFileData.size());
        }

        std::vector<uint8_t> outputData = read_file(mGlobal->tmpOutputFile);
        TS_ASSERT(outputData == testFileData);

    }

    void test_chain_ostream() {
        std::vector<uint8_t> testFileData = read_file(mGlobal->stringTestFile);
        {
            chain_ostream outputChain(file_ostream(mGlobal->tmpOutputFile));
            outputChain.push_new<buffer_ostream<chain_ostream::link_ostream>>();

            std::streamsize written = outputChain.write(testFileData.data(), testFileData.size());
            TS_ASSERT_EQUALS(written, testFileData.size());
        }

        std::vector<uint8_t> outputData = read_file(mGlobal->tmpOutputFile);
        TS_ASSERT(outputData == testFileData);
    }

    void test_async_ostream() {
        std::vector<uint8_t> testFileData = read_file(mGlobal->stringTestFile);
        {
            async_ostream<file_ostream> outputChain(file_ostream(mGlobal->tmpOutputFile));

            std::streamsize written = outputChain.write(testFileData.data(), testFileData.size());
            TS_ASSERT_EQUALS(written, testFileData.size());
        }

        std::vector<uint8_t> outputData = read_file(mGlobal->tmpOutputFile);
        TS_ASSERT(outputData == testFileData);
    }

    void test_out_operator() {
        heap_iostream<> stream;
        int num = 42;
        stream << num;
        num = 0;
        stream.seek(0);
        stream >> num;
        TS_ASSERT_EQUALS(num, 42);

        float pif = M_PI;
        float fnum = M_PI;
        stream.clear();
        stream << fnum;
        stream.seek(0);
        fnum = 100;
        stream >> fnum;
        TS_ASSERT_EQUALS(fnum, pif);

        double pi_d = M_PI;
        double dnum = M_PI;
        stream.clear();
        stream << fnum;
        fnum = 100;
        stream >> fnum;
        TS_ASSERT_EQUALS(dnum, pi_d);
    }

    Fixture *mGlobal;
};

