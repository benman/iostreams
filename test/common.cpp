#include <chrono>

#include "common.hpp"

double monotonicSeconds() {
    typedef std::chrono::steady_clock clock;
    typedef std::chrono::duration<double, std::ratio<1> > seconds;

    static std::chrono::time_point<clock> refSeconds;
    static bool needsInit = true;
    static seconds lastResult;
    if(needsInit) {
        refSeconds = clock::now();
        lastResult = clock::now() - refSeconds;
        needsInit = false;
    }
    seconds diff = clock::now() - refSeconds;

    if(diff > lastResult) {
        lastResult = diff;
    } else {
        diff = lastResult;
    }

    return diff.count();
}


ElapsedTime::ElapsedTime() {
    start();
}

void ElapsedTime::start() { mSecondsRef = monotonicSeconds(); }
double ElapsedTime::elapsedSeconds() {
    return monotonicSeconds() - mSecondsRef;
}


std::string to_string(const PerformanceResult& result) {
    std::stringstream stream;
    stream << "min " << result.minSeconds << " max " << result.maxSeconds;
    stream << " median " << result.medianSeconds;
    stream << " average " << result.averageSeconds;
    stream << " fps " << result.fps();
    stream << " total iterations " << result.timeResults.size();
    return stream.str();
}
