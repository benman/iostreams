#include <string>
#include <vector>
#include <assert.h>
#include <algorithm>
#include <sstream>

inline std::string testDir(std::string file) {
    // assume it's absolute in this case
    if(file[0] == '/') {
        return file;
    }
    std::string path = TEST_DIR;

    if(path[path.length()-1] != '/') {
        path += '/';
    }

    path += file;
    return path;
}


inline std::string tmpDir(std::string file) {
    // assume it's absolute in this case
    if(file[0] == '/') {
        return file;
    }
    std::string path = TMP_DIR;

    if(path[path.length()-1] != '/') {
        path += '/';
    }

    path += file;
    return path;
}


inline std::streamsize fileSize(const std::string file) {
    FILE*fp = fopen(file.c_str(), "rb");
    if(fp == nullptr) {
        return 0;
    }
    fseek(fp, 0, SEEK_END);
    std::streamsize size = ftell(fp);
    fclose(fp);
    return size;
}


class PerformanceResult {
public:
    explicit PerformanceResult(const std::vector<double> &times) {
        assert(times.size() > 0);

        timeResults = times;
        std::sort(timeResults.begin(), timeResults.end());
        medianSeconds = timeResults[timeResults.size()/2];

        minSeconds = times[0];
        maxSeconds = minSeconds;
        averageSeconds = 0;
        for(double result : timeResults) {
            averageSeconds += result;
            minSeconds = std::min(minSeconds, result);
            maxSeconds = std::max(maxSeconds, result);
        }
        averageSeconds /= (double) timeResults.size();
    }

    double fps() const { return 1.0/averageSeconds; }
    std::vector<double> timeResults;

    double minSeconds;
    double maxSeconds;
    double medianSeconds;
    double averageSeconds;
};

std::string to_string(const PerformanceResult& result);

double monotonicSeconds();

class ElapsedTime {
public:
    /** automatically calls start() */
    ElapsedTime();

    /** Begin measuring time from now */
    void start();
    /** @return elapsed time in seconds since last call to start() */
    double elapsedSeconds();
private:
    double mSecondsRef;
};

template<typename Function>
PerformanceResult testSpeed(const Function &fun) {
    std::vector<double> time;

    ElapsedTime totalTime;
    // the minimum
    for(int i = 0; i < 5; ++i) {
        double start = monotonicSeconds();
        fun();
        time.push_back(monotonicSeconds() - start);
    }

    // get atleast 2 seconds worth of data
    while(totalTime.elapsedSeconds() < 2.0) {
        double start = monotonicSeconds();
        fun();
        time.push_back(monotonicSeconds() - start);
    }

    return PerformanceResult(time);
}


template<typename Stream>
class crap_istream {
public:
    crap_istream(Stream &&stream) : mStream(std::move(stream)) {
    }

    std::streamsize read(void *data, std::streamsize size) {
        if(size <= 0) {
            return 0;
        }
        size = rand() % (size+1);
        if(size <= 1) {
            size = 1;
        }
        return mStream.read(data, size);
    }
    Stream mStream;
};

template<typename Stream>
class crap_ostream {
public:
    crap_ostream(Stream &&stream) : mStream(std::move(stream)) {
    }

    std::streamsize write(const void *data, std::streamsize size) {
        if(size <= 0) {
            return 0;
        }
        size = rand() % (size+1);
        if(size <= 1) {
            size = 1;
        }
        return mStream.write(data, size);
    }
    Stream mStream;
};
