#include <iostream>
#include <type_traits>
#include <sys/types.h>
#include <unistd.h>
#include <list>
#include <cxxtest/TestSuite.h>

#include <iostream/async_stream.hpp>
#include <iostream/file_stream.hpp>
#include <iostream/block_stream.hpp>
#include <iostream/buffer_stream.hpp>
#include <iostream/virtual_stream.hpp>

#include <iostream/iostream.hpp>
#include <iostream/sub_stream.hpp>
#include <iostream/stream_utility.hpp>


#include "common.hpp"

using namespace ios;

class Fixture {
public:
    Fixture() {
        stringTestFile = testDir("testFile.txt");
        tmpOutputFile = tmpDir("tmpOutput");
    }
    std::string stringTestFile;
    std::string tmpOutputFile;
};
class MyTestSuite : public CxxTest::TestSuite
{
public:

    // CXXTest doesn't allow constructors & destructors for a suite
    static MyTestSuite *createSuite() {
        MyTestSuite *suite = new MyTestSuite();
        suite->init();
        return suite;
    }

    static void destroySuite(MyTestSuite *suite) {
        suite->release();
        delete suite;
    }

    void init() {
        mGlobal = new Fixture();
    }

    void release() {
        delete mGlobal;
    }


    void test_memory_init_spead() {
        constexpr int size = 1024*1024*10;
        {
            // warm up the memory
            memory_iostream<> stream(size*sizeof(int));
        }
        PerformanceResult speed = testSpeed([size] {
            memory_iostream<> stream(size*sizeof(int));
        });

        std::cout << "\n";
        std::cout << "memory_iostream init performance of " << size << " ints\n";
        std::cout << "    " << to_string(speed) << '\n';

        PerformanceResult heapSpeed = testSpeed([size] {
            heap_iostream<> stream;
        });
        std::cout << "heap_iostream init performance of " << size << " ints\n";
        std::cout << "    " << to_string(heapSpeed) << '\n';

        PerformanceResult vectorSpeed = testSpeed([size] {
            std::vector<int> vector;
            vector.reserve(size);
        });

        std::cout << "std::vector insert performance of " << size << " ints\n";
        std::cout << "    " << to_string(vectorSpeed) << '\n';

        double ratio = speed.averageSeconds/vectorSpeed.averageSeconds;
        std::cout << "memory_iostream is " << ratio << "x slower std::vector\n";
        // allow some variability. Most of the time goes to allocating memory
        // which is out of our control
        TS_ASSERT(ratio < 1.1);
    }

    void test_memory_speed() {
        constexpr int size = 1024*1024*10;
        PerformanceResult insertSpeed = testSpeed([size] {
            memory_iostream<> stream(size*sizeof(int));

            for(int i = 0; i < size; ++i) {
                stream << i;
            }
            TS_ASSERT_EQUALS(stream.tell(), size*sizeof(int));

        });

        std::cout << "\n";
        std::cout << "memory_iostream insert performance of " << size << " ints\n";
        std::cout << "    " << to_string(insertSpeed) << '\n';

        PerformanceResult vectorInsertSpeed = testSpeed([size] {
            std::vector<int> vector;
            vector.reserve(size);

            for(int i = 0; i < size; ++i) {
                vector.push_back(i);
            }
            TS_ASSERT_EQUALS(vector.size(), size);

        });

        std::cout << "std::vector insert performance of " << size << " ints\n";
        std::cout << "    " << to_string(vectorInsertSpeed) << '\n';

        double ratio = insertSpeed.averageSeconds/vectorInsertSpeed.averageSeconds;
        std::cout << "memory_iostream is " << ratio << "x slower std::vector\n";
        TS_ASSERT(ratio < 0.58);
    }

    void test_heap_speed() {
        constexpr int size = 1024*1024*10;
        PerformanceResult insertSpeed = testSpeed([size] {
            heap_iostream<> stream;
            stream.set_max_block_size(size*sizeof(int));
            stream.reserve(size*sizeof(int));
            for(int i = 0; i < size; ++i) {
                stream << i;
            }
            TS_ASSERT_EQUALS(stream.tell(), size*sizeof(int));
        });

        std::cout << "\n";
        std::cout << "heap_iostream insert performance of " << size << " ints\n";
        std::cout << "    " << to_string(insertSpeed) << '\n';

        PerformanceResult vectorInsertSpeed = testSpeed([size] {
            std::vector<int> vector;
            vector.reserve(size);

            for(int i = 0; i < size; ++i) {
                vector.push_back(i);
            }
            TS_ASSERT_EQUALS(vector.size(), size);

        });

        std::cout << "std::vector insert performance of " << size << " ints\n";
        std::cout << "    " << to_string(vectorInsertSpeed) << '\n';

        PerformanceResult vectorNoReserveSpeed = testSpeed([size] {
            std::vector<int> vector;

            for(int i = 0; i < size; ++i) {
                vector.push_back(i);
            }
            TS_ASSERT_EQUALS(vector.size(), size);
        });

        std::cout << "std::vector insert performance of " << size << " ints no reserved memory\n";
        std::cout << "    " << to_string(vectorNoReserveSpeed) << '\n';


        PerformanceResult insertNoReserveSpeed = testSpeed([size] {
            heap_iostream<> stream;
            for(int i = 0; i < size; ++i) {
                stream << i;
            }
            TS_ASSERT_EQUALS(stream.tell(), size*sizeof(int));
        });
        std::cout << "heap_iostream insert performance of " << size << " ints no reserved memory\n";
        std::cout << "    " << to_string(insertNoReserveSpeed) << '\n';

        PerformanceResult listNoReserveSpeed = testSpeed([size] {
            std::list<int> vector;

            for(int i = 0; i < size; ++i) {
                vector.push_back(i);
            }
            TS_ASSERT_EQUALS(vector.size(), size);
        });

        std::cout << "std::list insert performance of " << size << " ints no reserved memory\n";
        std::cout << "    " << to_string(listNoReserveSpeed) << '\n';

        double ratio = insertSpeed.averageSeconds/vectorInsertSpeed.averageSeconds;
        std::cout << "heap_iostream is " << ratio << "x slower std::vector\n";
        TS_ASSERT(ratio < 1.1);
        TS_ASSERT(listNoReserveSpeed.fps() < insertSpeed.fps());
    }

    Fixture *mGlobal;
};

