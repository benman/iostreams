#include <iostream/iostream.hpp>
#include <iostream/file_stream.hpp>
#include <iostream/buffer_stream.hpp>
#include <iostream>
#include <iostream/line_text.hpp>

int main(int argc, char **argv) {
    std::string testFile = TEST_DIR;
    testFile += "/testFile.txt";
    std::string fileName = argc == 2? argv[1] : testFile;
    std::cout << "openning " << fileName << "\n";
    // Most streams are not buffered by default. You must wrap a buffer yourself.
    ios::buffer_istream<ios::file_istream> stream{ios::file_istream{fileName}};
    char c=0;
    std::streamsize didRead = stream.read(&c, 1);
    assert(didRead == 1);
    std::cout << "read " << didRead << " bytes\n";
    for(std::string line : ios::line_itext<decltype(stream)>(std::move(stream), "\n")) {
        // do something with line. "\n" is not appended to line variable.
        std::cout << line << "\n";
    }

}
