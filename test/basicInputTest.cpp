#include <iostream>
#include <type_traits>
#include <sys/types.h>
#include <unistd.h>
#include <cxxtest/TestSuite.h>

#include <iostream/async_stream.hpp>
#include <iostream/file_stream.hpp>
#include <iostream/block_stream.hpp>
#include <iostream/buffer_stream.hpp>
#include <iostream/virtual_stream.hpp>

#include <iostream/iostream.hpp>
#include <iostream/sub_stream.hpp>
#include <iostream/stream_utility.hpp>
#include <iostream/zero_stream.hpp>
#include <iostream/line_text.hpp>

#include "common.hpp"

using namespace ios;

class Fixture {
public:
    Fixture() {
        stringTestFile = testDir("testFile.txt");
    }
    std::string stringTestFile;
};
class MyTestSuite : public CxxTest::TestSuite
{
public:

    // CXXTest doesn't allow constructors & destructors for a suite
    static MyTestSuite *createSuite() {
        MyTestSuite *suite = new MyTestSuite();
        suite->init();
        return suite;
    }

    static void destroySuite(MyTestSuite *suite) {
        suite->release();
        delete suite;
    }

    void init() {
        mGlobal = new Fixture();
    }

    void release() {
        delete mGlobal;
    }

    void testAddition( void )
    {
        TS_ASSERT( 1 + 1 > 1 );
        TS_ASSERT_EQUALS( 1 + 1, 2 );
    }

    void test_TEST_DIR() {
        TS_ASSERT_EQUALS(TEST_DIR[0], '/');
    }

    void test_statics() {
#define READ_TEST(type, result, message) static_assert(has_read<type>::value == result, message); \
        TS_ASSERT_EQUALS(has_read<type>::value, result)

        READ_TEST(file_iostream, true, "file_iostream read does not conform");
        READ_TEST(file_istream, true, "file_istream read does not conform");
        READ_TEST(heap_iostream<>, true, "heap_iostream read does not conform");
        READ_TEST(memory_iostream<>, true, "memory_iostream<> read does not conform");
        READ_TEST(buffer_iostream<heap_iostream<>>, true, "buffer_iostream read does not conform");

#define WRITE_TEST(type, result, message) static_assert(has_write<type>::value == result, message); \
        TS_ASSERT_EQUALS(has_write<type>::value, result)

        WRITE_TEST(file_ostream, true, "file_iostream write does not conform");
        WRITE_TEST(heap_iostream<>, true, "heap_iostream write does not conform");
        WRITE_TEST(memory_iostream<>, true, "memory_iostream<> write does not conform");
        WRITE_TEST(buffer_iostream<heap_iostream<>>, true, "buffer_iostream write does not conform");
        std::ostream *istream = nullptr;
        std::istream *ostream = nullptr;

        (void)istream;
        (void)ostream;

        READ_TEST(decltype(std::cin), false, "cin must not have conforming read");
        WRITE_TEST(decltype(std::cout), false, "cout must not have conforming write");
        WRITE_TEST(std::ostream, false, "std::ostream should not conform to write");
        READ_TEST(std::istream, false, "std::istream should not conform to read");


    }
    int countLines(uint8_t* it, uint8_t* end) {
        int lines = 0;
        while(it != end) {
            if(*it == '\n') {
                ++lines;
            }
            ++it;
        }
        return lines;
    }

    void testMemoryStream() {
        memory_iostream<> stream(1024);
        stream.skip_write(100);
        TS_ASSERT_EQUALS(stream.tell(), 100);
        TS_ASSERT_EQUALS(stream.size(), 100);
        TS_ASSERT(stream.capacity() == 1024);
    }

    template<typename T>
    void simpleInputStreamTest(std::vector<uint8_t> &expectedOutput, T &&stream) {
        std::vector<uint8_t> loadedFileData;
        int linesRead = 0;
        for(std::string line : enumerate<line_iterator<T>>(stream, "\n")) {
            // so ineffeciant
            for(std::size_t i = 0; i < line.size(); ++i) {
                loadedFileData.push_back(line[i]);
            }
            loadedFileData.push_back('\n');
            ++linesRead;
        }

        std::vector<uint8_t> data(loadedFileData.size());
        std::copy(loadedFileData.begin(), loadedFileData.end(), data.begin());

        TS_ASSERT(expectedOutput == data);
        int lines = countLines(expectedOutput.data(), &expectedOutput[expectedOutput.size()-1]+1);
        TS_ASSERT_EQUALS(lines, linesRead);
    }

    template<typename T>
    void simpleCrapTest(std::vector<uint8_t> &expectedOutput, T &&stream) {
        crap_istream<T> crap(std::move(stream));
        simpleInputStreamTest(expectedOutput, crap);
    }

    void testAsync() {
        std::vector<uint8_t> fileData = read_file(mGlobal->stringTestFile);
        auto generateStream = [&] () -> async_istream<file_istream> {
            file_istream inputFile(mGlobal->stringTestFile);
            TS_ASSERT(inputFile.is_open());
            return async_istream<file_istream>(std::move(inputFile));
        };
        simpleInputStreamTest(fileData, generateStream());
        simpleCrapTest(fileData, generateStream());
    }

    void testBlockInputStream() {
        std::vector<uint8_t> fileData = read_file(mGlobal->stringTestFile);
        auto generateStream = [&] () -> block_istream<file_istream> {
            file_istream inputFile(mGlobal->stringTestFile);
            TS_ASSERT(inputFile.is_open());
            return block_istream<file_istream>(std::move(inputFile), 512);
        };
        simpleInputStreamTest(fileData, generateStream());
        simpleCrapTest(fileData, generateStream());
    }

    void testBlockInputStream111() {
        // this is to test a weird block size
        std::vector<uint8_t> fileData = read_file(mGlobal->stringTestFile);
        auto generateStream = [&] () -> block_istream<file_istream> {
            file_istream inputFile(mGlobal->stringTestFile);
            TS_ASSERT(inputFile.is_open());
            return block_istream<file_istream>(std::move(inputFile), 111);
        };
        simpleInputStreamTest(fileData, generateStream());
        simpleCrapTest(fileData, generateStream());
    }

    void testBufferInputStream() {
        // this is to test a weird block size
        std::vector<uint8_t> fileData = read_file(mGlobal->stringTestFile);
        TS_ASSERT(fileData.size() > 0);
        auto generateStream = [&] () -> buffer_istream<file_istream> {
            file_istream inputFile(mGlobal->stringTestFile);
            TS_ASSERT(inputFile.is_open());
            return buffer_istream<file_istream>(std::move(inputFile));
        };
        simpleInputStreamTest(fileData, generateStream());
        simpleCrapTest(fileData, generateStream());

        // test off by 1 error
        char c=0;
        auto stream = generateStream();
        std::streamsize didRead = stream.read(&c, 1);
        TS_ASSERT_EQUALS(didRead, 1);
        TS_ASSERT_EQUALS(c, fileData[0]);

        {
            ios::buffer_istream<ios::file_istream> stream(std::move(ios::file_istream(mGlobal->stringTestFile)));
            c = 0;
            didRead = stream.read(&c, 1);
            TS_ASSERT_EQUALS(didRead, 1);
            TS_ASSERT_EQUALS(c, fileData[0]);
        }
    }

    void testChainInputStream() {
        // this is to test a weird block size
        std::vector<uint8_t> fileData = read_file(mGlobal->stringTestFile);
        auto generateStream = [&] () -> chain_istream {
            file_istream inputFile(mGlobal->stringTestFile);
            TS_ASSERT(inputFile.is_open());
            chain_istream input(std::move(inputFile));
            input.push_new<buffer_istream<chain_istream::link_istream>>();
            return input;
        };
        simpleInputStreamTest(fileData, generateStream());
        simpleCrapTest(fileData, generateStream());
    }

    void testCopy() {
        // this is to test a weird block size
        std::vector<uint8_t> fileData = read_file(mGlobal->stringTestFile);

        file_istream inputFile(mGlobal->stringTestFile);
        TS_ASSERT(inputFile.is_open());
        buffer_istream<file_istream> input(std::move(inputFile));
        heap_iostream<> output;
        copy_result result = copy_stream(input, output);
        TS_ASSERT_EQUALS(result.total_write, result.total_read);
        TS_ASSERT(result.success);

        TS_ASSERT_EQUALS(output.tell(), result.total_write);
        TS_ASSERT_EQUALS(output.tell(), output.size());
        TS_ASSERT_EQUALS(output.tell(), fileData.size());
        std::streamsize size = output.tell();
        output.seek(0);
        std::vector<uint8_t> copied(size);
        std::streamsize data_read = output.read(copied.data(), copied.size());
        TS_ASSERT_EQUALS(data_read, copied.size());
        TS_ASSERT_EQUALS(copied.size(), size);
        TS_ASSERT(copied == fileData);
        TS_ASSERT_EQUALS(data_read, fileSize(mGlobal->stringTestFile));
    }

    void testCopy2() {
        // this is to test a weird block size
        std::vector<uint8_t> fileData = read_file(mGlobal->stringTestFile);

        file_istream inputFile(mGlobal->stringTestFile);
        TS_ASSERT(inputFile.is_open());
        buffer_istream<file_istream> inputPrecursor(std::move(inputFile));
        crap_istream<decltype(inputPrecursor)> input(std::move(inputPrecursor));
        heap_iostream<> output;
        copy_result result = copy_stream(input, output);
        TS_ASSERT_EQUALS(result.total_write, result.total_read);
        TS_ASSERT(result.success);

        TS_ASSERT_EQUALS(output.tell(), result.total_write);
        TS_ASSERT_EQUALS(output.tell(), output.size());
        TS_ASSERT_EQUALS(output.tell(), fileData.size());
        std::streamsize size = output.tell();
        output.seek(0);
        std::vector<uint8_t> copied(size);
        std::streamsize data_read = output.read(copied.data(), copied.size());
        TS_ASSERT_EQUALS(data_read, copied.size());
        TS_ASSERT_EQUALS(copied.size(), size);
        TS_ASSERT(copied == fileData);
        TS_ASSERT_EQUALS(data_read, fileSize(mGlobal->stringTestFile));
    }

    void testEndian() {
        TS_ASSERT_DIFFERS(is_little_endian(), is_big_endian());
    }

    void test_heap_iostream() {
        std::vector<uint8_t> testFileData = read_file(mGlobal->stringTestFile);
        heap_iostream<> stream;
        stream.write(testFileData.data(), testFileData.size());
        TS_ASSERT_EQUALS(stream.size(), testFileData.size());
        TS_ASSERT_EQUALS(stream.tell(), stream.size());
        stream.seek(0);
        TS_ASSERT_EQUALS(stream.tell(), 0);
        stream.seek(100);
        TS_ASSERT_EQUALS(stream.tell(), 100);
        stream.seek(testFileData.size());
        TS_ASSERT_EQUALS(stream.tell(), testFileData.size());
        stream.seek(0);
        stream.seek(0, std::ios::end);
        TS_ASSERT_EQUALS(stream.tell(), testFileData.size());

        stream << "hello world";
        stream.clear();
        int x = 0x12415113;
        // its empty so x should stay the same
        stream >> x;
        TS_ASSERT_EQUALS(x, 0x12415113);
    }

    void test_load_file() {
        std::vector<uint8_t> testFileData = read_file(mGlobal->stringTestFile);

        FILE *fp = fopen(mGlobal->stringTestFile.c_str(), "rb");
        fseek(fp, 0, SEEK_END);
        std::streamsize size = ftell(fp);
        fseek(fp, 0, SEEK_SET);
        std::vector<uint8_t> fileData;
        fileData.resize(size);
        std::streamsize read = fread(fileData.data(), 1, size, fp);
        fclose(fp);

        TS_ASSERT_EQUALS(read, size);
        TS_ASSERT_EQUALS(size, testFileData.size());
        TS_ASSERT(testFileData == fileData);
    }

    void test_to_vector() {
        std::vector<uint8_t> output = read_file(mGlobal->stringTestFile);
        file_istream input(mGlobal->stringTestFile);
        std::vector<uint8_t> output2;
        output2.resize(output.size());
        std::streamsize bytesRead = input.read(output2.data(), output2.size());
        TS_ASSERT_EQUALS(bytesRead, output2.size());
        TS_ASSERT(output2 == output);
    }

    // test_null_stream & test_zero_stream is really there just to make sure
    // it compiles. And no one does anything weird.
    void test_null_stream() {
        null_iostream null_stream;
        std::vector<uint8_t> data(315);
        TS_ASSERT_EQUALS(null_stream.write(data.data(), data.size()), data.size());
        TS_ASSERT_EQUALS(null_stream.read(data.data(), data.size()), 0);
    }

    void test_zero_stream() {
        zero_iostream zero_stream;
        std::vector<uint8_t> data(321);
        for(uint8_t &value : data) {
            value = 42;
        }
        TS_ASSERT_EQUALS(zero_stream.write(data.data(), data.size()), data.size());
        for(uint8_t &value : data) {
            TS_ASSERT_EQUALS(value, 42);
        }

        TS_ASSERT_EQUALS(zero_stream.read(data.data(), data.size()), data.size());
        for(uint8_t &value : data) {
            TS_ASSERT_EQUALS(value, 0);
        }
    }

    void test_in_operator() {
        heap_iostream<> stream;

        stream << "hello world";
        TS_ASSERT_EQUALS(stream.size(), std::strlen("hello world"));
    }
    void testSubStream() {
        std::vector<uint8_t> fileData = read_file(mGlobal->stringTestFile);

    }

    void test_empty_iteration() {

        // This file shouldn't exist
        ios::buffer_istream<ios::file_istream> stream(std::move(ios::file_istream("invalid")));
        for(std::string line : ios::line_itext<decltype(stream)>(std::move(stream))) {
            TS_FAIL("shouldn't reach here");
        }
    }
    Fixture *mGlobal;
};

