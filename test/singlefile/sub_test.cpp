

#include <iostream/sub_stream.hpp>
#include <cxxtest/TestSuite.h>

using namespace ios;

class null_iostream_tellable : public null_iostream {
public:
    std::streamsize write(const void *buf, std::streamsize size) {
        (void)buf;
        mOffset += size;
        return size;
    }
    std::streamoff tell() const {
        return mOffset;
    }
    std::streamsize mOffset = 0;
};

class MyTestSuite : public CxxTest::TestSuite
{
public:

    void testAddition( void )
    {
        TS_ASSERT( 1 + 1 > 1 );
        TS_ASSERT_EQUALS( 1 + 1, 2 );
    }

    void test_TEST_DIR() {
        TS_ASSERT_EQUALS(TEST_DIR[0], '/');
    }

    void test_basic() {
        null_iostream null_stream;
        sub_istream<null_iostream> nullSub(null_stream, 200);
        sub_ostream<null_iostream> null_o(null_stream, 29);

        char buffer[256];
        for(int i = 0; i < 256; ++i) {
            buffer[i] = i+1;
        }
        std::streamsize read = nullSub.read(buffer, 256);
        TS_ASSERT_EQUALS(read, 0);
        for(int i = 0; i < read; ++i) {
            TS_ASSERT_EQUALS(buffer[i], 0);
        }
        TS_ASSERT_EQUALS(null_o.write(nullptr, 214412), 29);

    }

    void test_basic_iostream() {
        char buffer[256];
        null_iostream_tellable null_stream;
        sub_iostream<null_iostream_tellable> nullIo(null_stream, 0, 242);
        std::streamsize transfered = nullIo.write(buffer, 333);
        TS_ASSERT_EQUALS(transfered, 242);
    }
};

