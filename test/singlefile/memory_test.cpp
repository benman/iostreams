

#include <iostream/memory_stream.hpp>
#include <cxxtest/TestSuite.h>

using namespace ios;


class MyTestSuite : public CxxTest::TestSuite
{
public:

    void testAddition( void )
    {
        TS_ASSERT( 1 + 1 > 1 );
        TS_ASSERT_EQUALS( 1 + 1, 2 );
    }

    void test_TEST_DIR() {
        TS_ASSERT_EQUALS(TEST_DIR[0], '/');
    }

    void test_basic() {
        heap_iostream<> heap;

        heap.write("hello world", 11);
        TS_ASSERT_EQUALS(heap.size(), 11);
    }

    void test_empty_seek() {
        heap_iostream<> heap;
        TS_ASSERT_EQUALS(heap.size(), 0);
        TS_ASSERT_EQUALS(heap.tell(), 0);
        heap.seek(12);
        TS_ASSERT_EQUALS(heap.tell(), 0);
    }

    void test_read() {
        heap_iostream<> input;
        input << "hello world";

        std::vector<uint8_t> data(1024);
        std::streamsize copySize = input.read(&data[0], data.size());
        TS_ASSERT_EQUALS(copySize, 0);
        input.seek(0);
        copySize = input.read(&data[0], data.size());
        TS_ASSERT_EQUALS(copySize, 11);
        std::string str = (const char*)data.data();
        TS_ASSERT_EQUALS(str, "hello world");
    }
};

