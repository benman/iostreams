

#include <iostream/iostream.hpp>
#include <cxxtest/TestSuite.h>


using namespace ios;


/** Just like /dev/null nothing when read. Accepts everything and discards. */
class simple_iostream : public iostream_base {
public:
    simple_iostream()                                 = default;
    simple_iostream           (const simple_iostream&)  = default;
    simple_iostream           (simple_iostream&&)       = default;
    simple_iostream& operator=(const simple_iostream&)  = default;
    simple_iostream& operator=(simple_iostream&&)       = default;

    /* These are so we can use it in a chain stream */
    template<typename T>
    explicit simple_iostream(const T&){}
    template<typename T>
    explicit simple_iostream(T&&){}

    /** @return 0 always. */
    std::streamsize read(void *buffer, std::streamsize size) {
        (void)buffer; (void)size;
        return 0;
    }
    /** does nothing and returns size like it actually written it fully

        @param buffer This buffer is ignored
        @param size  Will be returned back as if actually written @p size bytes.
        @return always returns @p size
    */
    std::streamsize write(const void*buffer, std::streamsize size) {
        (void)buffer;
        return size;
    }
};


class MyTestSuite : public CxxTest::TestSuite
{
public:

    void testAddition( void )
    {
        TS_ASSERT( 1 + 1 > 1 );
        TS_ASSERT_EQUALS( 1 + 1, 2 );
    }

    void test_TEST_DIR() {
        TS_ASSERT_EQUALS(TEST_DIR[0], '/');
    }

    void test_basic() {
        null_iostream null_stream;
        copy_result result = copy_stream(null_stream, null_stream);
        TS_ASSERT_EQUALS(result.total_read, 0);
        TS_ASSERT_EQUALS(result.total_write, 0);
        TS_ASSERT_EQUALS(result.success, false);
    }

    void test_operator() {
        null_iostream null_stream;

        int x = 32;
        null_stream >> x;
        TS_ASSERT_EQUALS(x, 32);
        x = 99;
        null_stream << x;

        simple_iostream simple;

        simple >> x;
        int32_t i32;

        null_stream >> i32;
        simple >> i32;
    }
};

