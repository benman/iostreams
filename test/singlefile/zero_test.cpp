

#include <iostream/zero_stream.hpp>
#include <cxxtest/TestSuite.h>

using namespace ios;


class MyTestSuite : public CxxTest::TestSuite
{
public:

    void testAddition( void )
    {
        TS_ASSERT( 1 + 1 > 1 );
        TS_ASSERT_EQUALS( 1 + 1, 2 );
    }

    void test_TEST_DIR() {
        TS_ASSERT_EQUALS(TEST_DIR[0], '/');
    }

    void test_basic() {
        zero_iostream zero;

        TS_ASSERT_EQUALS(zero.write(nullptr, 23781), 23781);
        uint8_t buffer[256];
        for(int i = 0; i < 256; ++i) {
            buffer[i] = i + 1;
        }

        zero.read(buffer, 256);
        for(int i = 0; i < 256; ++i) {
            TS_ASSERT_EQUALS(buffer[i], 0);
        }
    }

    void test_operator() {
        zero_iostream zero;

        int x = 32;
        zero >> x;
        TS_ASSERT_EQUALS(x, 0);
        x = 99;
        zero << x;
    }
};

