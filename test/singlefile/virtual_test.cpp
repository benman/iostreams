

#include <iostream/virtual_stream.hpp>
#include <cxxtest/TestSuite.h>

using namespace ios;
class MyTestSuite : public CxxTest::TestSuite
{
public:
    void testAddition( void )
    {
        TS_ASSERT( 1 + 1 > 1 );
        TS_ASSERT_EQUALS( 1 + 1, 2 );
    }

    void test_TEST_DIR() {
        TS_ASSERT_EQUALS(TEST_DIR[0], '/');
    }

    void test_basic() {
        null_iostream null_stream;

        chain_ostream chain(std::move(null_stream));
        chain.push_new<null_iostream>();

        std::streamsize written = chain.write(nullptr, 123123);
        TS_ASSERT_EQUALS(written, 123123);
        TS_ASSERT_EQUALS(null_stream.read(nullptr, 4123), 0);
        TS_ASSERT_EQUALS(chain.write(nullptr, 21412), 21412);
    }
};

