

#include <iostream/buffer_stream.hpp>
#include <cxxtest/TestSuite.h>

using namespace ios;


class MyTestSuite : public CxxTest::TestSuite
{
public:

    void testAddition( void )
    {
        TS_ASSERT( 1 + 1 > 1 );
        TS_ASSERT_EQUALS( 1 + 1, 2 );
    }

    void test_TEST_DIR() {
        TS_ASSERT_EQUALS(TEST_DIR[0], '/');
    }

    void test_basic() {
        /// @todo test single file buffer_stream
    }
};

