#include <iostream>
#include <type_traits>
#include <sys/types.h>
#include <unistd.h>
#include <cxxtest/TestSuite.h>

#include <iostream/async_stream.hpp>
#include <iostream/file_stream.hpp>
#include <iostream/block_stream.hpp>
#include <iostream/buffer_stream.hpp>
#include <iostream/virtual_stream.hpp>

#include <iostream/iostream.hpp>
#include <iostream/sub_stream.hpp>
#include <iostream/stream_utility.hpp>


#include "common.hpp"

using namespace ios;

class Fixture {
public:
    Fixture() {
        stringTestFile = testDir("testFile.txt");
        tmpOutputFile = tmpDir("tmpOutput");
    }
    std::string stringTestFile;
    std::string tmpOutputFile;
};
class MyTestSuite : public CxxTest::TestSuite
{
public:

    // CXXTest doesn't allow constructors & destructors for a suite
    static MyTestSuite *createSuite() {
        MyTestSuite *suite = new MyTestSuite();
        suite->init();
        return suite;
    }

    static void destroySuite(MyTestSuite *suite) {
        suite->release();
        delete suite;
    }

    void init() {
        mGlobal = new Fixture();
    }

    void release() {
        delete mGlobal;
    }

    template<typename Stream>
    void seek_test_template(Stream& stream) {
        constexpr int size = 1024*1024*10;
        for(int i = 0; i < size; ++i) {
            stream << i;
        }
        TS_ASSERT_EQUALS(stream.tell(), size*sizeof(int));

        // very taxing on some streams so lets not do it too much.
        // 100 is alot for some streams.
        auto testPoint = [&] (int where) -> bool {
            stream.seek(where*sizeof(int));
            TS_ASSERT_EQUALS(stream.tell(), where*sizeof(int));
            int result;
            stream >> result;
            TS_ASSERT_EQUALS(result, where);
            return result != where;
        };
        for(int i = 0; i < 100; ++i){
            int where = rand() % size;
            if(!testPoint(where)){
                break;
            }
        }

        testPoint(size/2);
        testPoint(size - sizeof(int));
        testPoint(0);
        testPoint(size/4);
        testPoint(size/2 + size/4);
        testPoint(size/3);
        stream.seek(0);
        std::streamoff skipped = skip(stream, 413);
        TS_ASSERT_EQUALS(skipped, 413);
    }

    void test_memory_iostream() {
        memory_iostream<> stream(1024*1024*10*sizeof(int));
        seek_test_template(stream);
    }
    void test_heap_iostream() {
        heap_iostream<> stream;
        seek_test_template(stream);
    }

    void test_file_iostream() {
        std::string fileName = TMP_DIR;
        fileName += "/seekTestfile.bin";
        file_iostream stream(fileName);
        seek_test_template(stream);
    }

    void test_buffer_iostream() {
        heap_iostream<> heap;
        buffer_iostream<heap_iostream<>> stream(std::move(heap));
        seek_test_template(stream);
    }

    Fixture *mGlobal;
};

