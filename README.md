# Goal & Target Design

is to create next generation input output for C++ standard library.

## Considerations

- Overload << >> for different types without needing to care about
  underlaying stream class.
    - I tried it, and the errors are way too cryptic and too hard
      to debug. This is not a good idea. There needs to be a wrapper
      class for this to work well. And the templates for wrapper class
      just get way to verbose to be practical. Just write/read functions
      are the way to go. write/read is very understandable.
- Runtime flexibility. Trade off between flexibility vs performance
- Keep basic streams fully binary (no text translations). Text/string & platform
  specific translations should be in a higher level filter.
- vector::reserve style buffering.
- asynchronous streams.
- All streams that are bufferable should have a means of disabling buffering.
  Ideally no streams should implement buffering themselves.
- When reading or writing data more than the size of buffering. It should
  passthrough directly to read/write calls instead of doing it in
  bufferable chunks. Thus reducing operating system calls.
- specify custom memory section to be used for buffering. Owned or unowned
  by stream.



## General Todo list

- Use naming convention of standard c++ libraries
- use acceptable style guidelines for standard c++ libraries
- Allow custom allocaters everywhere.
- too many includes in main header 1 header.
- use a namespace while not in standard e.g. ios?
- Write some tests.
- Generate documentation. Perhaps doxygen.
- we need a Data<> kind of structure. I could not think of anything better
  and std::vector<uint8_t> seems like it can work but main issue is it forces a copy
  to be made. Which is ok I guess.

## Naming convention for c++ standard

So I did not find any official guidelines for naming convention. So below
is my guess based on existing standard names.

- pure binary streams to end in `_istream`, `_ostream`, `_iostream`. e.g.
  async_istream, async_ostream. buffer_iostream, file_iostream.
- text/locale streams to have `_istring`, `_ostring`, `_iostring`. E.g.
  `LineReader` will become `line_istring`.

## List of stream classes

* `_(i/o/io)stream` a combination of i, o, or io meaning it should have
  `_istream`, `_ostream`, and or `_iostream`.
* `implemented` Y/N if it's implemented in this repository. i/o/io if only
  a specific one is implemented at the moment.
* `named` Y for is using target naming convention, N for no.
* `test cases` Y/N if it has adaquate test cases and they are passing.

| Class Prefix  | `_(i/o/io)stream`   | implemented | named | test cases
|---------------|:-------------------:|:-----------:|:-----:|:---------:
| `file`        |  i/o/io             |     Y       |  Y    | N
| `buffer`      |  i/o                |     Y       |  Y    | N
| `block`       |  i/o/io             |     Y       |  Y    | N
| `chain`       |  i/o                |     Y       |  Y    | N
| `async`       |  i/o                |     Y       |  Y    | N
| `ref`         |  i/o                |     Y       |  Y    | N
| `ptr`         |  i/o                |     Y       |  Y    | N
| `p`           |  i/o (io?)          |     Y       |  Y    | N
| `heap`        |  io                 |     Y       |  Y    | Y
| `memory`      |  io                 |     Y       |  Y    | Y
| `rewind`      |  i/o                |     N       |  N    | N
| `sub`         |  i/o/io             |     Y       |  Y    | N
| `zero`        |  io                 |     Y       |  Y    | Y
| `null`        |  io                 |     Y       |  Y    | Y

- file_i/o/iostream - basic file input/output
- buffer_i/o/iostream - implements buffering
- block_i/ostream - to guarantee reads of fixed block sizes
- chain_i/ostream - similar of boost filter. Create a list of streams that will
  be chained together.
- async_i/ostream - a buffered stream that does read/write ahead on a separate
  thread. Direct read/write calls are blocking. Maybe a better neame is
  ahead_i/ostream.
- ref_i/ostream - a stream holder to prevent another stream taking full
  ownership. This way you can wrap a custom inputstream temporarily. Usefull
  when a stream encodes multiple file for example. Or when you want to rechain.
- p_i/ostream - All methods are virtual so that templates no longer needed.
- p_i/ostream_sub<> - to wrap an input stream quickly inside p_istream &
  p_ostream.
- heap_i/o/iostream - read/write to a growable memory location.
- memory_i/o/iostream - read/write to a continuous memory region. heap/memory
  streams have too similar names. I don't know what to do about it.
- rewind_i/ostream - set a mark so that you can return back to where mark is.
  This is to replace the "put back" paradigm.
- sub_i/o/iostream - to handle a portion of a stream. E.g. when reading a chunk
  size then you want ensure a function not read more than a specific size.

## Variables
| Name      | Implemented   | test cases
|-----------|---------------|-----------
| bin       |      Y        | N
| bout      |      Y        | N
| berr      |      Y        | N

* bin/bout/berr are just binary versions of cin, cout, cerr.

## List of locale based streams

Locale will end in suffixes `_istring`, `_ostring`, `_iostring`.

| Class Prefix  | `_(i/o/io)string`     | implemented | named | test cases
|---------------|:-------------------:|:-----------:|:-----:|:---------:
| `line`        |  i/o                |     Y       |  N    | N
| `word`        |  i/o                |     N       |  N    | N


- line_i/ostring - reads a line at a time. Supports line_iterator so you can
  use in for loops.
- word_i/ostring - a reader that reads words at a time separated by a delimiter
  such as a space.

## List of functions

| Name                  |implemented | test cases
|-----------------------|:----------:|:-----------:
| `copy`                |      Y     | N
| `read_fully`          |      Y     | N
| `write_fully`         |      Y     | N
| `read_file`           |      Y     | N
| `to_string`           |      N     | N
| `to_vector`           |      Y     | N
| `is_big_endian`       |      Y     | N
| `to_big_endian`       |      Y     | N
| `is_little_endian`    |      Y     | N
| `to_little_endian`    |      Y     | N
| `is_network_endian`   |      Y     | N
| `to_network_endian`   |      Y     | N
| `from_network_endian` |      Y     | N
| `swap_endian`         |      Y     | N
| `skip`                |      Y     | N


- copy(input&, output&, copy_config&) - copy input stream to output stream.
  copy_config to give details about copying such as block size. Max bytes to
  copy, and other parameters.
- vector<> read_fully(InputStream&, estimateSize=implementation defined sensible
  default) - reads a stream fully and returns a Data object.
- vector<> read_file(std::string&) - reads a file fully and returns a Data
  object of contents.
- std::string to_string(vector<>) - convert Data to string (assuming UTF-8 maybe?)
- skip(stream, nbytes) - skips nbytes of data
- to_big_endian(int_type)/to_little_endian/swap_endian - the first 2 swap only
  if current endian doesn't match. Last obviously always swaps. Returns the
  respective endian representation. The to_little/big_endian needs a better
  name. As the same function is used when saving & loading.
- is_big_endian/is_little_endian - (constexpr not possible) check for big/little
  endian.
- to_vector - fully read stream and return a std::vector<uint8_t> of its
  contents.


## Overload << & >>

The overloads will have basic one for POD types. For pure binary streams it will
write the raw bytes of the POD. For istring/ostring it will convert to string
and write as such similar how cout/cin work by default.
