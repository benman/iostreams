from subprocess import call
from subprocess import Popen
import subprocess
import glob
import os

script_dir = os.path.dirname(os.path.realpath(__file__))
os.chdir(os.path.join(script_dir, "../"))

cpp_files = glob.glob("src/**/*.hpp", recursive=True)
cpp_files += glob.glob("src/**/*.cpp", recursive=True)

pipe = Popen(["cppcheck", "-q", "--enable=all", *cpp_files],
    universal_newlines=True,
    stderr=subprocess.PIPE)

has_error = False
for line in pipe.stderr:
    pass_line = True
    if "is never used." in line:
        pass_line = False
    if "error" in line:
        pass_line = True
        has_error = True
    else:
        pass_line = False
    if pass_line:
        print(line, end='')
pipe.wait()
if pipe.returncode != 0:
    has_error = True
exit(1 if has_error else 0)